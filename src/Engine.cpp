#include "main.hpp"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ===================================================================================================================================
// Engine.cpp
// -----------------------------------------------------------------------------------------------------------------------------------
// Class that links together the different parts of the game and allows main to run it.
// -----------------------------------------------------------------------------------------------------------------------------------
// Angela Gross
// NipNapped Dungeon
// ===================================================================================================================================

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// CONSTRUCTOR
Engine::Engine(int screenWidth, int screenHeight) : screenWidth(screenWidth), screenHeight(screenHeight), fovRadius(FOV_RADIUS), 
        level(FIRST_LEVEL), gameStatus(STARTUP) 
{
    TCODConsole::initRoot(screenWidth, screenHeight, WINDOW_TITLE, IS_FULLSCREEN);
    
    // Get level
    if(level % LAST_LEVEL != 0)
        levelType = (LevelCode)(level % LAST_LEVEL);
    else
        levelType = LAST_LEVEL;
    
    // Generate the GUI
    gui = new GUI();
}

// DESTRUCTOR
Engine::~Engine() 
{   
    term();
    delete gui;
}

// TERMINATION
void Engine::term()
{
    actors.clearAndDelete();
    if (map) delete map;
    gui->clear();
    level = FIRST_LEVEL; // reset level
}

// INITIALIZATION
void Engine::init()
{
    // Make and add player
    player = new Actor(PLAYER_DEFAULT_X, PLAYER_DEFAULT_Y, PLAYER_CHAR, PLAYER_NAME, PLAYER_COLOR);
    player->destructible = new PlayerDestructible(PLAYER_MAX_HEALTH, PLAYER_DEFENSE, PLAYER_CORPSE_NAME, PLAYER_BASE_XP_DROP, !IS_BOSS);
    player->attacker = new Attacker(PLAYER_ATTACK);
    player->ai = new PlayerAI();
    player->container = new Container(INVENTORY_SIZE);
    actors.push(player);
    
    // Make and add stairs
    stairs = new Actor(0, 0, STAIRS_CHAR, STAIRS_NAME, STAIRS_COLOR);
    stairs->blocks = false;
    stairs->fovOnly = false;
    actors.push(stairs);
    
    // Generate the map
    map = new Map(SCREEN_WIDTH, SCREEN_HEIGHT - PANEL_HEIGHT);
    map->init(true);
   
    // Add beginning message and start game.
    gui->message(TCODColor::crimson, "Prepare to perish in the Catacombs of Kitty The Gray.");   
    gui->clearInventoryConsole();
    gameStatus = STARTUP;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ==================================================================================================================================
// UPDATE()
// ----------------------------------------------------------------------------------------------------------------------------------
// Updates all actors' statuses and states.
// ==================================================================================================================================
void Engine::update()
{    
    // Computer field of view for the first frame of the game.
    if (gameStatus == STARTUP) 
        map->computeFov();
    
    // Change to idle and lister for key press and mouse movement.
    gameStatus = IDLE;
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
    
    // Pause to menu
    if ( lastKey.vk == TCODK_ESCAPE ) 
    {
        save();
        load();
    }
    
    // Update the player
    player->update();
    
    // Loop through all the actors and update (except the player)
    if (gameStatus == NEW_TURN) 
    {
        for (Actor** iterator = actors.begin(); iterator != actors.end(); iterator++) 
        {
            Actor* actor = *iterator;
            
            if (actor != player)
                actor->update();
        }
    }
}

// ==================================================================================================================================
// RENDER()
// ----------------------------------------------------------------------------------------------------------------------------------
// Clears the console and draws the map and the actors.
// ==================================================================================================================================
void Engine::render()
{
    // Clear console and draw map.
    TCODConsole::root->clear();
    map->render();
    
    // Only draw actors if they are in the player's FOV and isn't the player.
    for (Actor** iterator = actors.begin(); iterator != actors.end(); iterator++) 
    {
        Actor* actor = *iterator;
        
        if ( (actor != player) && ( (!actor->fovOnly && map->isExplored(actor->x, actor->y) ) || map->isInFov(actor->x, actor->y) ) ) 
            actor->render();
    }
    
    // Draw player and the player's statistics  
    player->render();
    gui->render();
}

// ==================================================================================================================================
// SENDTOFRONT()
// ----------------------------------------------------------------------------------------------------------------------------------
// Send the actor to the front of the list.
// ==================================================================================================================================
void Engine::sendToFront(Actor* actor)
{
    actors.remove(actor);
    actors.insertBefore(actor, 0);
}

// ==================================================================================================================================
// GETCLOSESTMONSTER()
// ----------------------------------------------------------------------------------------------------------------------------------
// Get the closest monster in range. If range is 0, then it's considered infinite.
// ==================================================================================================================================
Actor* Engine::getClosestMonster(int x, int y, float range) const
{
    Actor* closest = NULL;
    float bestDistance = 1E6f;
    
    // Loop over all actors
    for (Actor** iterator = actors.begin(); iterator != actors.end(); iterator++) 
    {
        Actor* actor = *iterator;
        
        // If it's alive, see if it's closer to the target and in range.
        if ( actor != player && actor->destructible && !actor->destructible->isDead() )
        {
            float distance = actor->getDistance(x,y);
            if ( distance < bestDistance && ( distance <= range || range == 0.0f ) ) 
            {
                bestDistance = distance;
                closest = actor;
            }
        }
    }
    
    return closest;
}

// ==================================================================================================================================
// PICKATILE()
// ----------------------------------------------------------------------------------------------------------------------------------
// Creates another main game loop so user can pick a tile.
// ==================================================================================================================================
bool Engine::pickATile(int* x, int* y, float maxRange) 
{
    while ( !TCODConsole::isWindowClosed() ) 
    {
        // Draw map and actors
        render();
        
        // Highlight where user can pick.
        for (int cx = 0; cx < map->width; cx++) 
        {
            for (int cy = 0; cy < map->height; cy++) 
            {
                if ( map->isInFov(cx, cy) && ( maxRange == 0 || player->getDistance(cx, cy) <= maxRange) ) 
                {
                    TCODColor color = TCODConsole::root->getCharBackground(cx,cy);
                     color = color * 1.2f;
                    TCODConsole::root->setCharBackground(cx, cy, color);
                }
            }
        }
        
        // Wait for key or mouse
        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
        
        // Fill the title under mouse cursor with white
        if ( map->isInFov(mouse.cx,mouse.cy) && ( maxRange == 0 || player->getDistance(mouse.cx, mouse.cy) <= maxRange) )
        {
            TCODConsole::root->setCharBackground(mouse.cx,mouse.cy,TCODColor::white);
        
            // If left button pressed, return and update coordinates.
            if (mouse.lbutton_pressed) 
            {
                *x = mouse.cx;
                *y = mouse.cy;

                return true;
            }
            
            // Cancel if a key was pressed or the right button is pressed.
            if (mouse.rbutton_pressed || lastKey.vk != TCODK_NONE) 
                return false;       
        }
        
        TCODConsole::flush();
    }
    
    // Exit if main window closed
    return false;
}

// ==================================================================================================================================
// GETACTOR()
// ----------------------------------------------------------------------------------------------------------------------------------
// Retrieves an actor if found at the specified coordinates.
// ==================================================================================================================================
Actor* Engine::getActor(int x, int y) const
{
    // Loop over all actors
    for (Actor** iterator = actors.begin(); iterator != actors.end(); iterator++) 
    {
        Actor* actor = *iterator;
        
        // Found actor
        if(actor->x == x && actor->y == y)
            return actor;
    }
    
    // Didn't find actor
    return NULL;
}

// ==================================================================================================================================
// NEXTLEVEL()
// ----------------------------------------------------------------------------------------------------------------------------------
// Proceed to the next level.
// ==================================================================================================================================
void Engine::nextLevel()
{
    // Increase level
    level++;
    
    // Get level type
    if(level % LAST_LEVEL != 0)
        levelType = (LevelCode)(level % LAST_LEVEL);
    else
        levelType = LAST_LEVEL;
    
    // Monsters scale with every <LAST_LEVEL> levels
    engine.levelUpMonsters();
    
    // Is it the final boss?
    bool finalBoss = (level % LAST_LEVEL == 0);
    
    // Notify user
    gui->message(TCODColor::lightViolet, "You take a moment to lick your wounds and take a cat nap.");
    player->destructible->heal(player->destructible->maxHp/2);
    
    // Add another message to notify user of level
    if(finalBoss)
        gui->message(TCODColor::red, "You get a feeling that Kitty The Gray is lurking \n around this dungeon. Be prepared.");
    else
        gui->message(TCODColor::red, "After that nice rest, you descend and continue, \n so you can get your cat nip at last...");
    
    // Remove the map and all actors except for the stairs and the player
    delete map;
    for (Actor** iterator = actors.begin(); iterator != actors.end(); iterator++) 
    {
        if (*iterator != player && *iterator != stairs) 
        {
            delete *iterator;
            iterator = actors.remove(iterator);
        }
    }
    
    // Generate a new map
    map = new Map(SCREEN_WIDTH, SCREEN_HEIGHT - 7);
    
    // Only the main boss
    if(finalBoss)
        map->bossMap = true;

    // Initialize and create map
    map->init(true);
    
    // Restart the level
    gameStatus = STARTUP;
}

// ==================================================================================================================================
// LEVELUPMONSTERS()
// ----------------------------------------------------------------------------------------------------------------------------------
// Make monsters more difficult to kill as the levels go on
// ==================================================================================================================================
void Engine::levelUpMonsters()
{
    // So we can still have the same monsters if we reload the game
    int scale = level / LAST_LEVEL;
    
    // Don't need to change anything
    if(scale == 0)
        return;
    
    for(int i = 0; i <= LAST_MONSTER; i++)
    {
        MonsterType monster = (MonsterType)i;
        
        // Increase attributes of monsters
        switch(monster)
        {
            case MOUSE:
            {
                MOUSE_MAX_HEALTH = scale * LEVEL_UP_MONSTERS[i][HEALTH];
                MOUSE_ATTACK = scale * LEVEL_UP_MONSTERS[i][ATTACK];
                MOUSE_DEFENSE = scale * LEVEL_UP_MONSTERS[i][DEFENSE];
                MOUSE_BASE_XP_DROP = scale * LEVEL_UP_MONSTERS[i][XP];
            }
            break;
            case PUPPY:
            {
                PUPPY_MAX_HEALTH = scale * LEVEL_UP_MONSTERS[i][HEALTH];
                PUPPY_ATTACK = scale * LEVEL_UP_MONSTERS[i][ATTACK];
                PUPPY_DEFENSE = scale * LEVEL_UP_MONSTERS[i][DEFENSE];
                PUPPY_BASE_XP_DROP = scale * LEVEL_UP_MONSTERS[i][XP];
            }
            break;
            case DOG:
            {
                DOG_MAX_HEALTH = scale * LEVEL_UP_MONSTERS[i][HEALTH];
                DOG_ATTACK =  scale * LEVEL_UP_MONSTERS[i][ATTACK];
                DOG_DEFENSE = scale * LEVEL_UP_MONSTERS[i][DEFENSE];
                DOG_BASE_XP_DROP = scale * LEVEL_UP_MONSTERS[i][XP];
            }
            break;
            case VACUUM:
            {
                VACUUM_MAX_HEALTH = scale * LEVEL_UP_MONSTERS[i][HEALTH];
                VACUUM_ATTACK =  scale * LEVEL_UP_MONSTERS[i][ATTACK];
                VACUUM_DEFENSE = scale * LEVEL_UP_MONSTERS[i][DEFENSE];
                VACUUM_BASE_XP_DROP = scale * LEVEL_UP_MONSTERS[i][XP];
            }
            break;
            case KITTY_THE_GRAY:
            {
                KITTY_THE_GRAY_MAX_HEALTH = scale * LEVEL_UP_MONSTERS[i][HEALTH];
                KITTY_THE_GRAY_ATTACK =  scale * LEVEL_UP_MONSTERS[i][ATTACK];
                KITTY_THE_GRAY_DEFENSE = scale * LEVEL_UP_MONSTERS[i][DEFENSE];
                KITTY_THE_GRAY_BASE_XP_DROP = scale * LEVEL_UP_MONSTERS[i][XP];
            }
            break;
            default: break;
        }
    }
}
        

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

